#use wml::debian::translation-check translation="ec63203ae7c88b70ad002f6a6d89bd22297d4fb8" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Kaspersky Lab a découvert pjuusieurs vulnérabilités dans libvncserver, une
bibliothèque C pour mettre en œuvre les fonctions serveur/client de VNC.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-6307">CVE-2018-6307</a>

<p>Une vulnérabilité d’utilisation de tas après libération dans le code du
serveur de l’extension de transfert de fichiers peut aboutir à l’exécution
de code à distance. Cette attaque semble être exploitable à l’aide de la
connectivité réseau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-15127">CVE-2018-15127</a>

<p>Une vulnérabilité d’écriture de tas hors limites est contenue dans le code du
serveur de l’extension de transfert de fichiers, pouvant aboutir à
l’exécution de code à distance. Cette attaque semble être exploitable à l’aide
de la connectivité réseau.</p></li>


<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20019">CVE-2018-20019</a>

<p>Plusieurs vulnérabilités d’écriture de tas hors limites dans le code du
client VNC peuvent aboutir à l’exécution de code à distance.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20020">CVE-2018-20020</a>

<p>Plusieurs vulnérabilités d’écriture de tas hors limites dans une structure du
code de client VNC peuvent aboutir à l'exécution de code à distance.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20021">CVE-2018-20021</a>

<p>CWE-835 : vulnérabilité de boucle infinie dans le code du client VNC. Cette
vulnérabilité pourrait permettre à un attaquant d’utiliser un montant excessif
de ressources, comme le CPU et la RAM.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20022">CVE-2018-20022</a>

<p>CWE-665 : une faiblesse d’initialisation incorrecte dans le code du client
VNC pourrait permettre à un attaquant de lire la mémoire de pile et permettre la
divulgation d'informations. Combinée avec une autre vulnérabilité, elle peut
être utilisée pour divulguer la disposition de la mémoire de pile et contourner
ASLR.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20023">CVE-2018-20023</a>

<p>Une vulnérabilité d’initialisation incorrecte dans le code du client VNC
Repeater pourrait permettre à un attaquant de lire la mémoire de pile et
permettre la divulgation d'informations. Combinée avec une autre vulnérabilité,
elle peut être utilisée pour divulguer la disposition de la mémoire de pile et
contourner ASLR.</p></li>


<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20024">CVE-2018-20024</a>

<p>Un déréférencement de pointeur NULL dans le code du client VNC pourrait
aboutir à un déni de service.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans la version 0.9.9+dfsg2-6.1+deb8u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libvncserver.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1617.data"
# $Id: $
