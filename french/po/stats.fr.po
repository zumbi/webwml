# Templates files for stats modules
# Copyright (C) 2011 Software in the Public Interest, Inc.
#
# David Prévot <david@tilapin.org>, 2011, 2012.
# Jean-Paul Guillonneau <guillonneau.jeanpaul@free.fichier>, 2017-18.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"PO-Revision-Date: 2018-06-24 08:58+0200\n"
"Last-Translator: Jean-Paul Guillonneau <guillonneau.jeanpaul@free.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.2\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#: ../../english/template/debian/stats_tags.wml:6
msgid "Debian web site translation statistics"
msgstr "Statistiques de traduction du site web Debian"

#: ../../english/template/debian/stats_tags.wml:10
msgid "There are %d pages to translate."
msgstr "%d pages sont à traduire."

#: ../../english/template/debian/stats_tags.wml:14
msgid "There are %d bytes to translate."
msgstr "%d octets sont à traduire."

#: ../../english/template/debian/stats_tags.wml:18
msgid "There are %d strings to translate."
msgstr "%d chaînes sont à traduire."

#: ../../stattrans.pl:278 ../../stattrans.pl:494
msgid "Wrong translation version"
msgstr "Mauvaise version de traduction"

#: ../../stattrans.pl:280
msgid "This translation is too out of date"
msgstr "Cette traduction est trop incomplète"

#: ../../stattrans.pl:282
msgid "The original is newer than this translation"
msgstr "L'original est plus récent que sa traduction"

#: ../../stattrans.pl:286 ../../stattrans.pl:494
msgid "The original no longer exists"
msgstr "L'original n'existe plus"

#: ../../stattrans.pl:470
msgid "hit count N/A"
msgstr "Nombre de visites indisponible"

#: ../../stattrans.pl:470
msgid "hits"
msgstr "visites"

#: ../../stattrans.pl:488 ../../stattrans.pl:489
msgid "Click to fetch diffstat data"
msgstr "Cliquer pour récupérer les statistiques du différentiel"

#: ../../stattrans.pl:599 ../../stattrans.pl:739
msgid "Created with <transstatslink>"
msgstr "Créé avec <transstatslink>"

#: ../../stattrans.pl:604
msgid "Translation summary for"
msgstr "Vue globale des traductions pour la langue :"

#: ../../stattrans.pl:607 ../../stattrans.pl:763 ../../stattrans.pl:809
#: ../../stattrans.pl:852
msgid "Not translated"
msgstr "Non traduites"

#: ../../stattrans.pl:607 ../../stattrans.pl:762 ../../stattrans.pl:808
msgid "Outdated"
msgstr "Incomplètes"

#: ../../stattrans.pl:607
msgid "Translated"
msgstr "Traduites"

#: ../../stattrans.pl:607 ../../stattrans.pl:687 ../../stattrans.pl:761
#: ../../stattrans.pl:807 ../../stattrans.pl:850
msgid "Up to date"
msgstr "À jour"

#: ../../stattrans.pl:608 ../../stattrans.pl:609 ../../stattrans.pl:610
#: ../../stattrans.pl:611
msgid "files"
msgstr "fichiers"

#: ../../stattrans.pl:614 ../../stattrans.pl:615 ../../stattrans.pl:616
#: ../../stattrans.pl:617
msgid "bytes"
msgstr "octets"

#: ../../stattrans.pl:624
msgid ""
"Note: the lists of pages are sorted by popularity. Hover over the page name "
"to see the number of hits."
msgstr ""
"Remarque : les listes de pages sont classées par popularité. Le nombre de "
"visites apparaît lorsque le pointeur passe sur le nom de la page."

#: ../../stattrans.pl:630
msgid "Outdated translations"
msgstr "Traductions incomplètes"

#: ../../stattrans.pl:632 ../../stattrans.pl:686
msgid "File"
msgstr "Fichier"

#: ../../stattrans.pl:634
msgid "Diff"
msgstr "Différentiel"

#: ../../stattrans.pl:636
msgid "Comment"
msgstr "Commentaire"

#: ../../stattrans.pl:637
msgid "Diffstat"
msgstr "Statistiques du différentiel"

#: ../../stattrans.pl:638
msgid "Git command line"
msgstr "Ligne de commande de Git"

#: ../../stattrans.pl:640
msgid "Log"
msgstr "Journal"

#: ../../stattrans.pl:641
msgid "Translation"
msgstr "Traduction"

#: ../../stattrans.pl:642
msgid "Maintainer"
msgstr "Responsable"

#: ../../stattrans.pl:644
msgid "Status"
msgstr "État"

#: ../../stattrans.pl:645
msgid "Translator"
msgstr "Traducteur"

#: ../../stattrans.pl:646
msgid "Date"
msgstr "Date"

#: ../../stattrans.pl:653
msgid "General pages not translated"
msgstr "Pages générales non traduites"

#: ../../stattrans.pl:654
msgid "Untranslated general pages"
msgstr "Pages générales non traduites"

#: ../../stattrans.pl:659
msgid "News items not translated"
msgstr "Nouvelles non traduites"

#: ../../stattrans.pl:660
msgid "Untranslated news items"
msgstr "Nouvelles non traduites"

#: ../../stattrans.pl:665
msgid "Consultant/user pages not translated"
msgstr "Pages de consultants et d'utilisateurs non traduites"

#: ../../stattrans.pl:666
msgid "Untranslated consultant/user pages"
msgstr "Pages de consultants et d'utilisateurs non traduites"

#: ../../stattrans.pl:671
msgid "International pages not translated"
msgstr "Pages internationales non traduites"

#: ../../stattrans.pl:672
msgid "Untranslated international pages"
msgstr "Pages internationales non traduites"

#: ../../stattrans.pl:677
msgid "Translated pages (up-to-date)"
msgstr "Pages web traduites (à jour)"

#: ../../stattrans.pl:684 ../../stattrans.pl:834
msgid "Translated templates (PO files)"
msgstr "Chaînes traduites (fichiers PO)"

#: ../../stattrans.pl:685 ../../stattrans.pl:837
msgid "PO Translation Statistics"
msgstr "Statistiques de traduction des fichiers PO"

#: ../../stattrans.pl:688 ../../stattrans.pl:851
msgid "Fuzzy"
msgstr "Approximatives"

#: ../../stattrans.pl:689
msgid "Untranslated"
msgstr "Non traduites"

#: ../../stattrans.pl:690
msgid "Total"
msgstr "Total"

#: ../../stattrans.pl:707
msgid "Total:"
msgstr "Total :"

#: ../../stattrans.pl:741
msgid "Translated web pages"
msgstr "Pages web traduites"

#: ../../stattrans.pl:744
msgid "Translation Statistics by Page Count"
msgstr "Statistiques de traduction par nombre de pages"

#: ../../stattrans.pl:759 ../../stattrans.pl:805 ../../stattrans.pl:849
msgid "Language"
msgstr "Langue"

#: ../../stattrans.pl:760 ../../stattrans.pl:806
msgid "Translations"
msgstr "Traductions"

#: ../../stattrans.pl:787
msgid "Translated web pages (by size)"
msgstr "Pages web traduites (par taille)"

#: ../../stattrans.pl:790
msgid "Translation Statistics by Page Size"
msgstr "Statistiques de traduction par taille de pages"

#~ msgid "Unified diff"
#~ msgstr "Différentiel unifié"

#~ msgid "Colored diff"
#~ msgstr "Différentiel coloré"

#~| msgid "Colored diff"
#~ msgid "Commit diff"
#~ msgstr "Différentiel de commit"

#~ msgid "Created with"
#~ msgstr "Page créée avec"

#~ msgid "Origin"
#~ msgstr "Original"
