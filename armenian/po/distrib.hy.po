msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2006-08-12 17:27+0200\n"
"Last-Translator: unknown\n"
"Language-Team: unknown\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/distrib/search_contents-form.inc:9
#: ../../english/distrib/search_packages-form.inc:8
msgid "Keyword"
msgstr "Բառը"

#: ../../english/distrib/search_contents-form.inc:13
msgid "Display"
msgstr "Էկրան"

#: ../../english/distrib/search_contents-form.inc:16
msgid "paths ending with the keyword"
msgstr ""

#: ../../english/distrib/search_contents-form.inc:19
msgid "packages that contain files named like this"
msgstr "փաթեթներ, որոնք իրենց մեջ պարունակում են այսպիսի անունով ֆայլեր"

#: ../../english/distrib/search_contents-form.inc:22
#, fuzzy
msgid "packages that contain files whose names contain the keyword"
msgstr "փաթեթներ, որոնք իրենց մեջ պարունակում են այսպիսի անունով ֆայլեր"

#: ../../english/distrib/search_contents-form.inc:25
#: ../../english/distrib/search_packages-form.inc:25
msgid "Distribution"
msgstr "Թողարկման տիպը"

#: ../../english/distrib/search_contents-form.inc:27
#: ../../english/distrib/search_packages-form.inc:27
msgid "experimental"
msgstr ""

#: ../../english/distrib/search_contents-form.inc:28
#: ../../english/distrib/search_packages-form.inc:28
msgid "unstable"
msgstr ""

#: ../../english/distrib/search_contents-form.inc:29
#: ../../english/distrib/search_packages-form.inc:29
msgid "testing"
msgstr ""

#: ../../english/distrib/search_contents-form.inc:30
#: ../../english/distrib/search_packages-form.inc:30
msgid "stable"
msgstr ""

#: ../../english/distrib/search_contents-form.inc:31
#: ../../english/distrib/search_packages-form.inc:31
msgid "oldstable"
msgstr ""

#: ../../english/distrib/search_contents-form.inc:33
msgid "Architecture"
msgstr "Համակառույցը"

#: ../../english/distrib/search_contents-form.inc:38
#: ../../english/distrib/search_packages-form.inc:32
#: ../../english/distrib/search_packages-form.inc:39
msgid "any"
msgstr "ցանկացած"

#: ../../english/distrib/search_contents-form.inc:48
#: ../../english/distrib/search_packages-form.inc:43
msgid "Search"
msgstr "Փնտրել"

#: ../../english/distrib/search_contents-form.inc:49
#: ../../english/distrib/search_packages-form.inc:44
msgid "Reset"
msgstr "Մաքրել"

#: ../../english/distrib/search_packages-form.inc:12
msgid "Search on"
msgstr "Փնտրել"

#: ../../english/distrib/search_packages-form.inc:14
msgid "Package names only"
msgstr "Միայն փաթեթների անունների մեջ"

#: ../../english/distrib/search_packages-form.inc:16
msgid "Descriptions"
msgstr "Բացատրություններում"

#: ../../english/distrib/search_packages-form.inc:18
msgid "Source package names"
msgstr "Փաթեթների սկզբնաղբյուրների անուների մեջ"

#: ../../english/distrib/search_packages-form.inc:21
msgid "Only show exact matches"
msgstr ""

#: ../../english/distrib/search_packages-form.inc:34
msgid "Section"
msgstr "Բաժինը"

#: ../../english/distrib/search_packages-form.inc:36
msgid "main"
msgstr ""

#: ../../english/distrib/search_packages-form.inc:37
msgid "contrib"
msgstr ""

#: ../../english/distrib/search_packages-form.inc:38
msgid "non-free"
msgstr "ոչ ազատ"

#: ../../english/releases/arches.data:8
msgid "Alpha"
msgstr ""

#: ../../english/releases/arches.data:9
msgid "64-bit PC (amd64)"
msgstr ""

#: ../../english/releases/arches.data:10
msgid "ARM"
msgstr ""

#: ../../english/releases/arches.data:11
msgid "64-bit ARM (AArch64)"
msgstr ""

#: ../../english/releases/arches.data:12
msgid "EABI ARM (armel)"
msgstr ""

#: ../../english/releases/arches.data:13
msgid "Hard Float ABI ARM (armhf)"
msgstr ""

#: ../../english/releases/arches.data:14
msgid "HP PA-RISC"
msgstr ""

#: ../../english/releases/arches.data:15
msgid "Hurd 32-bit PC (i386)"
msgstr ""

#: ../../english/releases/arches.data:16
msgid "32-bit PC (i386)"
msgstr ""

#: ../../english/releases/arches.data:17
msgid "Intel Itanium IA-64"
msgstr ""

#: ../../english/releases/arches.data:18
msgid "kFreeBSD 32-bit PC (i386)"
msgstr ""

#: ../../english/releases/arches.data:19
msgid "kFreeBSD 64-bit PC (amd64)"
msgstr ""

#: ../../english/releases/arches.data:20
msgid "Motorola 680x0"
msgstr ""

#: ../../english/releases/arches.data:21
msgid "MIPS (big endian)"
msgstr ""

#: ../../english/releases/arches.data:22
msgid "64-bit MIPS (little endian)"
msgstr ""

#: ../../english/releases/arches.data:23
msgid "MIPS (little endian)"
msgstr ""

#: ../../english/releases/arches.data:24
msgid "PowerPC"
msgstr ""

#: ../../english/releases/arches.data:25
msgid "POWER Processors"
msgstr ""

#: ../../english/releases/arches.data:26
msgid "RISC-V 64-bit little endian (riscv64)"
msgstr ""

#: ../../english/releases/arches.data:27
msgid "IBM S/390"
msgstr ""

#: ../../english/releases/arches.data:28
msgid "IBM System z"
msgstr ""

#: ../../english/releases/arches.data:29
msgid "SPARC"
msgstr ""

#~ msgid "all files in this package"
#~ msgstr "այս փաթեթի բոլոր ֆայլերը"

#~ msgid "no"
#~ msgstr "ոչ"

#~ msgid "yes"
#~ msgstr "այո"

#~ msgid "non-US"
#~ msgstr "ոչ-US"
