msgid ""
msgstr ""
"PO-Revision-Date: 2016-04-24 17:00+0900\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/blends/blend.defs:15
msgid "Metapackages"
msgstr "メタパッケージ"

#: ../../english/blends/blend.defs:18
msgid "Downloads"
msgstr "ダウンロード"

#: ../../english/blends/blend.defs:21
msgid "Derivatives"
msgstr "派生物"

#: ../../english/blends/released.data:15
msgid ""
"The goal of Debian Astro is to develop a Debian based operating system that "
"fits the requirements of both professional and hobby astronomers. It "
"integrates a large number of software packages covering telescope control, "
"data reduction, presentation and other fields."
msgstr ""
"Debian Astro の目標は天文学者及び趣味で天体観測を行う人の両方の要求に応える "
"Debian ベースのオペレーティングシステムを開発することです。天体望遠鏡の制御や"
"データ整理、展示その他の領域を対象とする大量のソフトウェアパッケージを統合し"
"ます。"

#: ../../english/blends/released.data:23
msgid ""
"The goal of DebiChem is to make Debian a good platform for chemists in their "
"day-to-day work."
msgstr ""
"DebiChem の目標は Debian を化学者にとっての日々の研究に適した基盤にすることで"
"す。"

#: ../../english/blends/released.data:31
#, fuzzy
#| msgid ""
#| "The goal of Debian Games is to provide games in Debian from arcade and "
#| "aventure to simulation and strategy."
msgid ""
"The goal of Debian Games is to provide games in Debian from arcade and "
"adventure to simulation and strategy."
msgstr ""
"Debian Games の目標はアーケードや恋愛物からシミュレーションや戦略まで、ゲーム"
"を Debian に提供することです。"

#: ../../english/blends/released.data:39
msgid ""
"The goal of Debian Edu is to provide a Debian OS system suitable for "
"educational use and in schools."
msgstr ""
"Debian Edu の目標は教育での利用や学校に適する Debian OS システムを提供するこ"
"とです。"

#: ../../english/blends/released.data:47
msgid ""
"The goal of Debian GIS is to develop Debian into the best distribution for "
"Geographical Information System applications and users."
msgstr ""
"Debian GIS の目標は Debian を地理情報システム (<abbr title=\"Geographical "
"Information System\">GIS</abbr>) アプリケーションやそのユーザにとって最善の"
"ディストリビューションに開発することです。"

#: ../../english/blends/released.data:57
msgid ""
"The goal of Debian Junior is to make Debian an OS that children will enjoy "
"using."
msgstr "Debian Junior の目標は Debian を使うことが楽しいOSにすることです。"

#: ../../english/blends/released.data:65
msgid ""
"The goal of Debian Med is a complete free and open system for all tasks in "
"medical care and research. To achieve this goal Debian Med integrates "
"related free and open source software for medical imaging, bioinformatics, "
"clinic IT infrastructure, and others within the Debian OS."
msgstr ""
"Debian Med の目標は医療や研究に関わるあらゆるタスクに対応する完全にフリーで"
"オープンなシステムです。この目標を実現するため Debian Med は医学画像や生物情"
"報学、そして診療所のIT基盤等に関連するフリーでオープンソースのソフトウェアを "
"Debian OS に統合します。"

#: ../../english/blends/released.data:73
msgid ""
"The goal of Debian Multimedia is to make Debian a good platform for audio "
"and multimedia work."
msgstr ""
"Debian Multimedia の目標は Debian を音楽やマルチメディア向けに適した基盤にす"
"ることです。"

#: ../../english/blends/released.data:81
msgid ""
"The goal of Debian Science is to provide a better experience when using "
"Debian to researchers and scientists."
msgstr ""
"Debian Science の目標は研究者や科学者が Debian を使ったときにより良い経験を与"
"えることです。"

#: ../../english/blends/unreleased.data:15
msgid ""
"The goal of Debian Accessibility is to develop Debian into an operating "
"system that is particularly well suited for the requirements of people with "
"disabilities."
msgstr ""
"Debian Accessibility の目標は Debian を特に身体障害者の要求に合うオペレーティ"
"ングシステムに開発することです。"

#: ../../english/blends/unreleased.data:23
msgid ""
"The goal of Debian Design is to provide applications for designers. This "
"includes graphic design, web design and multimedia design."
msgstr ""
"Debian Design の目標はデザイナー向けのアプリケーションを提供することです。グ"
"ラフィックやウェブ、マルチメディア等のデザインが対象です。"

#: ../../english/blends/unreleased.data:30
msgid ""
"The goal of Debian EzGo is to provide culture-based open and free technology "
"with native language support and appropriate user friendly, lightweight and "
"fast desktop environment for low powerful/cost hardwares to empower human "
"capacity building and technology development  in many areas and regions, "
"like Africa, Afghanistan, Indonesia, Vietnam  using Debian."
msgstr ""
"Debian EzGo の目標は現地の言語をサポートする、適度に使いやすく非力、低価格な"
"ハードウェアでも軽量で高速なデスクトップ環境によって文化をベースとしたオープ"
"ンでフリーの技術を提供し、Debian を使って多くの領域での人間の能力強化や技術開"
"発をアフリカやアフガニスタン、インドネシア、ベトナム等多くの地域でできるよう"
"にすることです。"

#: ../../english/blends/unreleased.data:38
msgid ""
"The goal of FreedomBox is to develop, design and promote personal servers "
"running free software for private, personal communications. Applications "
"include blogs, wikis, websites, social networks, email, web proxy and a Tor "
"relay on a device that can replace a wireless router so that data stays with "
"the users."
msgstr ""
"FreedomBox の目標は私的、個人的な通信向けのフリーソフトウェアを利用した個人向"
"けのサーバを開発、設計、推進することです。対象アプリケーションにはブログや "
"wiki、ウェブサイト、ソーシャルネットワーク、電子メール、ウェブプロキシがあ"
"り、デバイス上の Tor リレーを無線ルータの代わりにできるためデータはユーザの下"
"に残ります。"

#: ../../english/blends/unreleased.data:46
msgid ""
"The goal of Debian Hamradio is to support the needs of radio amateurs in "
"Debian by providing logging, data mode and packet mode applications and more."
msgstr ""
"Debian Hamradio の目標はログ収集やデータモード及びパケットモードのアプリケー"
"ションその他を提供することで Debian におけるアマチュア無線での需要をサポート"
"することです。"

#: ../../english/blends/unreleased.data:55
msgid ""
"The goal of DebianParl is to provide applications to support the needs of "
"parliamentarians, politicians and their staffers all around the world."
msgstr ""
"DebianParl の目標は世界中の議員や政治家及び職員の需要をサポートするアプリケー"
"ションを提供することです。"

#~ msgid "Debian Games"
#~ msgstr "Debian ゲーム"

#~ msgid "Debian Accessibility"
#~ msgstr "Debian Accessibility"
