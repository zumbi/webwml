<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Cisco Talos discovered that hdf5, a file format and library for
storing scientific data, contained several vulnerabilities that could
lead to arbitrary code execution when handling untrusted data.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.8.8-9+deb7u1.</p>

<p>We recommend that you upgrade your hdf5 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-771.data"
# $Id: $
