<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>The following vulnerabilities have been discovered in the Debian
Wheezy's Wireshark version:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9373">CVE-2016-9373</a>

    <p>The DCERPC dissector could crash</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9374">CVE-2016-9374</a>

    <p>The AllJoyn dissector could crash</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9375">CVE-2016-9375</a>

    <p>The DTN dissector could ender an infinite loop</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9376">CVE-2016-9376</a>

    <p>The OpenFlow dissector could crash</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.12.1+g01b65bf-4+deb8u6~deb7u5.</p>

<p>We recommend that you upgrade your wireshark packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-714.data"
# $Id: $
