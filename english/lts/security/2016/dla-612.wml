<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that the implementation of RSA signature verification
in libtomcrypt is vulnerable to the Bleichenbacher signature attack.</p>

<p>If an RSA key with exponent 3 is used it may be possible to forge a
PKCS#1 v1.5 signature signed by that key.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.17-3.2+deb7u1.</p>

<p>We recommend that you upgrade your libtomcrypt packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-612.data"
# $Id: $
