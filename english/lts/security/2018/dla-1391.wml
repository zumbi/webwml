<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities have been discovered in the libtiff library and
the included tools, which may result in denial of service:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-11613">CVE-2017-11613</a>

    <p>Ddenial of service vulnerability in the TIFFOpen function. A crafted
    input will lead to a denial of service attack and can either make the
    system hand or trigger the OOM killer.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5784">CVE-2018-5784</a>

    <p>There is an uncontrolled resource consumption in TIFFSetDirectory function
    of src/libtiff/tif_dir.c, which can cause denial of service through a
    crafted tif file.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
4.0.2-6+deb7u21.</p>

<p>We recommend that you upgrade your tiff packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1391.data"
# $Id: $
