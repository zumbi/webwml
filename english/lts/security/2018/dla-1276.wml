<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Jonas Klempel discovered that, when parsing the AIA-Extension field of
a client certificate, Apache Tomcat Native did not correctly handle
fields longer than 127 bytes. The result of the parsing error was to
skip the
OCSP check. It was therefore possible for client certificates that
should have been rejected (if the OCSP check had been made) to be
accepted. Users not using OCSP checks are not affected by this
vulnerability.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.1.24-1+deb7u1.</p>

<p>We recommend that you upgrade your tomcat-native packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1276.data"
# $Id: $
